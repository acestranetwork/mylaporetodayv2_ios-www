
// GCM key = AIzaSyBW_Bise4m1Y4L9O_7McErTHksCrgiOuKw

var app_id='NBpeKz9Ukpy9LHpZJcHSZJWsQ';
var model='';
var device_token='';
var backbutton=0;
var set='';
var get_token='';
var app=angular.module('mylapore', ['ionic','ngCordova','ionic.rating','angular-momentjs','yaru22.angular-timeago'])
app.run(function($ionicPlatform,$cordovaGoogleAnalytics,acepush,mylaipush,$cordovaToast,$state,$ionicHistory,$timeout) {
  $ionicPlatform.ready(function() {
    if(!localStorage.getItem('setting') || localStorage.getItem('setting') == null ){
      set={'init':true,alert:true,font_size:'Medium'};
      localStorage.setItem('setting',JSON.stringify(set));
    }
    ionic.Platform.fullScreen();
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(false);
      }
    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        $cordovaToast.showLongBottom('Sorry, no Internet connectivity detected. Please reconnect and try again.');
     }
    }

    model=ionic.Platform.device();

    var push = PushNotification.init({"ios": { "alert": "true", "badge": "true", "sound": "true", "clearBadge": true }});
    push.on('registration', function(data) {
      device_token=data.registrationId;
      if(JSON.stringify(localStorage.getItem('device_token'))!= JSON.stringify(device_token)){
        localStorage.setItem('setting',true);
        localStorage.setItem('device_token',device_token);
        var obj={'app_id':app_id,'device_token':device_token,'model':model.model,'platform':model.platform,'status':true}
          acepush.notification(obj);
          mylaipush.notification(obj);
      }
    });

    push.on('notification', function(data) {
      var a=data.additionalData.additionalData.split("_");
      if(a[0]=="notification"){
        $state.go('notificationdetails',{'obj':{'page':a[0],'category':a[1],'id':a[2]}});
      }
    });

    if(typeof analytics !== 'undefined'){
      $cordovaGoogleAnalytics.debugMode();
      $cordovaGoogleAnalytics.startTrackerWithId('UA-91055211-1');
      $cordovaGoogleAnalytics.trackView('APP first screen');
    }
  });

});

app.config(function($stateProvider,$urlRouterProvider,$ionicConfigProvider,$httpProvider,$momentProvider) {
$ionicConfigProvider.tabs.position('top'); // other values: top
$ionicConfigProvider.backButton.previousTitleText(false).text('');
// $ionicConfigProvider.views.transition('none');
$ionicConfigProvider.scrolling.jsScrolling(false);
// $ionicSideMenuDelegate.canDragContent(false);
 $httpProvider.defaults.useXDomain = true;
  // Remove the header used to identify ajax call  that would prevent CORS from working
  delete $httpProvider.defaults.headers.common['X-Requested-With'];
  // Set api token
  if(typeof API_TOKEN !== 'undefined') {
    $httpProvider.defaults.headers.common['Authentication-Token'] = API_TOKEN;
  }
  $momentProvider
      .asyncLoading(false)
      .scriptUrl('moment.min.js');
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
 $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/tab.html',
    controller: 'AppCtrl'
  })
  .state('app.news', {
    url: '/news',
    views: {
      'tab-news': {
        templateUrl: 'templates/news.html',
        controller: 'NewsCtrl'
      }
    }
  })
  .state('app.classified', {
    url: '/classified',
    views: {
      'tab-classified': {
        templateUrl: 'templates/classified.html',
        controller: 'ClassifiedCtrl'
      }
    }
  })
  .state('app.directory', {
    url: '/directory',
    views: {
      'tab-directory': {
        templateUrl: 'templates/directory.html',
        controller: 'DirectoryCtrl'
      }
    }
  })
  .state('app.civic', {
    url: '/civic',
    views: {
      'tab-civic': {
        templateUrl: 'templates/civic.html',
        controller: 'CivicCtrl'
      }
    }
  })
  .state('app.event', {
    url: '/event',
    views: {
      'tab-event': {
        templateUrl: 'templates/event.html',
        controller: 'EventCtrl'
      }
    }
  })
  .state('app.tourism', {
    url: '/tourism',
    views: {
      'tab-tourism': {
        templateUrl: 'templates/tourism.html',
        controller: 'TourismCtrl'
      }
    }
  })
  .state('contact', {
    url: '/contact',
    templateUrl: 'templates/contact.html',
    controller: 'ContactCtrl'
  })
  .state('search', {
    url: '/search',
    params:{obj:null},
    cache:false,
    nativeTransitionsBackAndroid: {
      "type": "slide",
      "direction": "down"
    },
    nativeTransitions:{
          "type": "slide",
          "direction": "up"
    },
    templateUrl: 'templates/details.html',
    controller: 'SearchCtrl'
  })
  .state('vilambaram', {
    url: '/vilambaram',
    templateUrl: 'templates/vilambaram.html',
    controller: 'VilambaramCtrl'
  })

  .state('settings', {
    url: '/settings',
    cache:false,
    templateUrl: 'templates/settings.html',
    controller: 'SettingsCtrl'
  })
  .state('details', {
    url: '/details',
    params:{obj:null},
    nativeTransitionsBackAndroid: {
      "type": "slide",
      "direction": "down"
    },
    nativeTransitions:{
          "type": "slide",
          "direction": "up"
    },
    templateUrl: 'templates/details.html',
    controller: 'DetailsCtrl'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/news');
});

var url='https://www.mylaporetoday.com/api/';
var image_url='https://www.mylaporetoday.com/';
// var url='http://v4mylai.acecommunicate.com/welcome/';
// var image_url='http://v4mylai.acecommunicate.com/';
// var url='http://mylaievent/welcome/';
// var image_url='http://mylaievent/';
var ad=0;
var limit=17;
