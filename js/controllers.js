app.controller('AppCtrl', function($scope,$ionicModal, $window, $state,service, acepush,$cordovaToast, $ionicHistory, browser) {
  $scope.urls = url;
    $scope.iurls = image_url;
    $scope.woo = false;
    $scope.search_icon=true;
    $scope.searchsubmit=function(key){
    if(key){
        $scope.search_icon=!$scope.search_icon;
        $scope.search_value='';
        $state.go('search',{obj:key});
      }
    }
    $scope.searchview=function(){
      $scope.search_value='';
     $scope.search_icon=!$scope.search_icon;
    }
    $scope.height = $window.innerHeight - 108 + 'px';
    $scope.heights = $window.innerHeight - 60 + 'px';

    if (localStorage.getItem('setting')) {
      if (JSON.parse(localStorage.getItem('setting')).alert == true) {
        $scope.push = true;
      } else {
        $scope.push = false;
      }
      $scope.font = JSON.parse(localStorage.getItem('setting')).font_size;
    }

    $scope.theme = function() {
      if ($scope.style === "style1")
        $scope.style = "style2";
      else
        $scope.style = "style1";
    }

    $scope.gosearch=function(){
      $state.go('app.search');
    }

    $scope.back=function(){
      $state.go('app.news');
    }


    //Breaking news
    service.get(url + 'get_breaking_news').then(function(result) {
      var temp = '';
      if(result.data){
        angular.forEach(result.data.breaking, function(item) {
          temp = temp + item.news + '  &ast;  ';
        })
        $scope.breaking_news = temp;
        $scope.advertise = result.data.advertisement;
      }
      }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
      }
    });
})

app.controller('NewsCtrl', function($scope,service,$state,$http,$cordovaToast,$cordovaSocialSharing,$timeout,acepush,mylaipush) {
  var news_page_number = 1;
    $scope.news_nodata = false;
    $scope.news_loadmore = false;
    var obj = {
      limit: limit,
      page: news_page_number
    };
    // var obj1={'app_id':app_id,'device_token':'device_token','model':'remix','platform':'android','status':true}
    //   acepush.notification(obj1);
    //   mylaipush.notification(obj1);

    $scope.news_load_icon = true;
    service.post(url + 'get_news', obj).then(function(result) {
      if (result.status == "success") {
        $scope.news_load_icon = false;
        if (result.data == 0) {
          $scope.news_nodata = true;
          $scope.news_load_icon = false;
          $scope.news_loading_icon = false;
          $scope.news_loadmore = false;
        } else {
          $scope.news_load_icon = false;
          news_page_number = news_page_number + 1;
          $scope.news = result.data;
          $scope.news_loading_icon = true;
          $scope.news_loadmore = true;
        }
      } else if (result.status == "error") {
        $scope.news_loading_icon = false;
        $cordovaToast.showLongBottom('Network Failed' + result.message);
      }
    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.news_nodata = true;
        $scope.news_loading_icon = false;
        $scope.news_loadmore = false;
        $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });
/*$scope.urls = url;
    $scope.iurls = image_url;
 $scope.news_share = result.data;
     $scope.OtherShare=function(){
     window.plugins.socialsharing.share('Mylaporetoday', $scope.urls + $scope.news_share.title , $scope.iurls + $scope.news_share.image_path + '/' + $scope.news_share.image , 'https://play.google.com/store/apps/details?id=com.acestranetworks.mylaporetoday');
  }*/
    // Load more function
    $scope.news_load = function() {
      var loadnews = [];
      $scope.news_loading_icon = true;
      var objs = {
        limit: limit,
        page: news_page_number
      };
      service.post(url + 'get_news', objs).then(function(result) {
        if (result.status == "success") {
          news_page_number = news_page_number + 1;
          if (result.data == 0) {
            $scope.news_loading_icon = false;
            $scope.news_loadmore = false;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
          angular.forEach(result.data, function(item) {
            $scope.news.push(item);
          })
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.news_loading_icon = false;
        } else if (result.status == "error") {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.news_loading_icon = false;
          $cordovaToast.showLongBottom('Network Failed' + result.message);
        }
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        if (error == 500 || error == 404 || error == 0) {
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
    };
    //Refresh
    $scope.news_refresh = function() {
      $timeout(function() {
        $scope.news_load_icon = false;
        var news_page_number = 1;
        $scope.news_nodata = false;
        $scope.news_loadmore = false;
        var obj = {
          limit: limit,
          page: news_page_number
        };
        service.post(url + 'get_news', obj).then(function(result) {
          if (result.status == "success") {
            if (result.data == 0) {
              $scope.news_nodata = true;
              $scope.news_loading_icon = false;
              $scope.news_loadmore = false;
            } else {
              news_page_number = news_page_number + 1;
              $scope.news = result.data;
              $scope.news_loading_icon = true;
              $scope.news_loadmore = true;
            }
          } else if (result.status == "error") {
            $scope.news_loading_icon = false;
            $cordovaToast.showLongBottom('Network Failed' + result.message);
          }
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {
            $scope.news_nodata = true;
            $scope.news_loading_icon = false;
            $scope.news_loadmore = false;
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    }
    $scope.next=function(){
      $state.go('app.tourism');
    }
    $scope.prev=function(){
      $state.go('app.classified');
    }
    $scope.details=function(x){
        $state.go('details',{'obj':{'page':'notification','category':'News','id':x}});
    }
  /*  $scope.urls = url;
    $scope.iurls = image_url;
    $scope.surl = share_url;
    $scope.share = function(data) {
      console.log(data)
      $cordovaSocialSharing
        .share(data.description , data.title , $scope.iurls + data.image_path + '/' + data.image , $scope.surl + data.slug) // Share via native share sheet
        .then(function(result) {
        }, function(err) {
        });
    }*/
})

app.controller('ClassifiedCtrl', function($scope,$state,service,$cordovaToast,$timeout) {
  // Classified page
    var classified_page_number = 1;
    $scope.classified_nodata = false;
    $scope.classified_loadmore = false;
    $scope.classified_load_icon = true;
    var obj = {
      limit: limit,
      page: classified_page_number
    };
    service.post(url + 'get_classify', obj).then(function(result) {
      if (result.status == "success") {
        if (result.data == 0) {
          $scope.classified_nodata = true;
          $scope.classified_loading_icon = false;
          $scope.classified_loadmore = false;
          $scope.classified_load_icon = false;
        } else {
          $scope.classified_load_icon = false;
          classified_page_number = classified_page_number + 1;
          $scope.classified = result.data;
          $scope.classified_loading_icon = true;
          $scope.classified_loadmore = true;
        }
      } else if (result.status == "error") {
        $scope.classified_loading_icon = false;
        $cordovaToast.showLongBottom('Network Failed' + result.message);
      }
    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.classified_nodata = true;
        $scope.classified_loading_icon = false;
        $scope.classified_loadmore = false;
        $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });

    // Load more function
    $scope.classified_load = function() {
      $scope.classified_loading_icon = true;
      var objs = {
        limit: limit,
        page: classified_page_number
      };
      service.post(url + 'get_classify', objs).then(function(result) {
        if (result.status == "success") {
          classified_page_number = classified_page_number + 1;
          if (result.data == 0) {
            $scope.classified_loading_icon = false;
            $scope.classified_loadmore = false;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
          angular.forEach(result.data, function(item) {
            $scope.classified.push(item);
          })
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.classified_loading_icon = false;
        } else if (result.status == "error") {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.classified_loading_icon = false;
          $cordovaToast.showLongBottom('Network Failed' + result.message);
        }
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        if (error == 500 || error == 404 || error == 0) {
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
    };
    //Refresh
    $scope.classified_refresh = function() {
      $timeout(function() {
        $scope.classified_load_icon = true;
        var classified_page_number = 1;
        $scope.classified_nodata = false;
        $scope.classified_loadmore = false;
        var obj = {
          limit: limit,
          page: classified_page_number
        };
        service.post(url + 'get_classify', obj).then(function(result) {
          $scope.classified_load_icon = false;
          if (result.status == "success") {
            if (result.data == 0) {
              $scope.classified_nodata = true;
              $scope.classified_loading_icon = false;
              $scope.classified_loadmore = false;
            } else {
              classified_page_number = classified_page_number + 1;
              $scope.classified = result.data;
              $scope.classified_loading_icon = true;
              $scope.classified_loadmore = true;
            }
          } else if (result.status == "error") {
            $scope.classified_loading_icon = false;
            $cordovaToast.showLongBottom('Network Failed' + result.message);
          }
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {
            $scope.classified_nodata = true;
            $scope.classified_loading_icon = false;
            $scope.classified_loadmore = false;
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    }
    $scope.next=function(){
      $state.go('app.news');
    }
    $scope.prev=function(){
      $state.go('app.directory');
    }
})
app.controller('SearchCtrl', function($scope,$state,service,$cordovaToast,$timeout,$stateParams) {
  $scope.urls = url;
  $scope.iurls = image_url;
  $scope.page='Search';
  $scope.mytitle='Search';
    var datas=$stateParams.obj;
        $scope.not_found = false;
        $scope.search_load_icon=true;
        service.post(url + 'get_search', {
          key: datas
        }).then(function(result) {
          $scope.search_load_icon=false;
          if (result.status == "success") {
            if (result.data.length <= 0) {
              $scope.not_found = true;
            }
            $scope.search_value = '';
            $scope.detail = result.data;
          } else if (result.status == "error") {
            $cordovaToast.showLongBottom('Network Failed' + result.message);
          }
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
});
app.controller('DirectoryCtrl', function($scope,$state,service,$cordovaToast,$timeout) {
  // Directory page
  var directory_page_number = 1;
  $scope.directory_nodata = false;
  $scope.directory_loadmore = false;
  $scope.directory_load_icon = true;
  var obj = {
    limit: limit,
    page: directory_page_number
  };
  service.post(url + 'get_directory', obj).then(function(result) {
    if (result.status == "success") {
      if (result.data == 0) {
        $scope.directory_nodata = true;
        $scope.directory_loading_icon = false;
        $scope.directory_loadmore = false;
        $scope.directory_load_icon = false;
      } else {
        $scope.directory_load_icon = false;
        directory_page_number = directory_page_number + 1;
        $scope.directory = result.data;
        $scope.directory_loading_icon = true;
        $scope.directory_loadmore = true;
      }
    } else if (result.status == "error") {
      $scope.directory_loading_icon = false;
      $cordovaToast.showLongBottom('Network Failed' + result.message);
    }
  }, function(error) {
    if (error == 500 || error == 404 || error == 0) {
      $scope.directory_nodata = true;
      $scope.directory_loading_icon = false;
      $scope.directory_loadmore = false;
      $cordovaToast.showLongBottom('Network Failed' + error);
    }
  });

  // Load more function
  $scope.directory_load = function() {
    $scope.directory_loading_icon = true;
    var objs = {
      limit: limit,
      page: directory_page_number
    };
    service.post(url + 'get_directory', objs).then(function(result) {
      if (result.status == "success") {
        directory_page_number = directory_page_number + 1;
        if (result.data == 0) {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.directory_loading_icon = false;
          $scope.directory_loadmore = false;
        }
        angular.forEach(result.data, function(item) {
          $scope.directory.push(item);
        })
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.directory_loading_icon = false;
      } else if (result.status == "error") {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        $scope.directory_loading_icon = false;
        $cordovaToast.showLongBottom('Network Failed' + result.message);
      }
    }, function(error) {
      $scope.$broadcast('scroll.infiniteScrollComplete');
      if (error == 500 || error == 404 || error == 0) {
        $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });
  };
  $scope.directory_refresh = function() {
    $timeout(function() {
      $scope.directory_load_icon = true;
      var directory_page_number = 1;
      $scope.directory_nodata = false;
      $scope.directory_loadmore = false;
      var obj = {
        limit: limit,
        page: directory_page_number
      };
      service.post(url + 'get_directory', obj).then(function(result) {
          $scope.directory_load_icon = false;
        if (result.status == "success") {
          if (result.data == 0) {
            $scope.directory_nodata = true;
            $scope.directory_loading_icon = false;
            $scope.directory_loadmore = false;
          } else {
            directory_page_number = directory_page_number + 1;
            $scope.directory = result.data;
            $scope.directory_loading_icon = true;
            $scope.directory_loadmore = true;
          }
        } else if (result.status == "error") {
          $scope.directory_loading_icon = false;
          $cordovaToast.showLongBottom('Network Failed' + result.message);
        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          $scope.directory_nodata = true;
          $scope.directory_loading_icon = false;
          $scope.directory_loadmore = false;
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
      //Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    }, 1000);
  }
  $scope.detail=function(data){
    $state.go('app.details', {
      obj: {
        page:'Directory',
        objects: data
      }
    });
  }
  $scope.next=function(){
    $state.go('app.classified');
  }
  $scope.prev=function(){
    $state.go('app.civic');
  }
})

app.controller('CivicCtrl',function($scope,$state,service,$cordovaToast,$timeout) {
  // Civic Issue page
    var civicissue_page_number = 1;
    $scope.civicissue_nodata = false;
    $scope.civicissue_loadmore = false;
    $scope.civicissue_load_icon = true;
    var obj = {
      limit: limit,
      page: civicissue_page_number
    };
    service.post(url + 'get_civicissue', obj).then(function(result) {
      if (result.status == "success") {
        if (result.data == 0) {
          $scope.civicissue_nodata = true;
          $scope.civicissue_loading_icon = false;
          $scope.civicissue_loadmore = false;
          $scope.civicissue_load_icon = false;
        } else {
          $scope.civicissue_load_icon = false;
          civicissue_page_number = civicissue_page_number + 1;
          $scope.civicissue = result.data;
          $scope.civicissue_loading_icon = true;
          $scope.civicissue_loadmore = true;
        }
      } else if (result.status == "error") {
        $scope.civicissue_load_icon = false;
        $scope.civicissue_loading_icon = false;
        $cordovaToast.showLongBottom('Network Failed' + result.message);
      }
    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.civicissue_load_icon = false;
        $scope.civicissue_nodata = true;
        $scope.civicissue_loading_icon = false;
        $scope.civicissue_loadmore = false;
        $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });
    var obj_address = function(line_one, category, sub_category, sales, profit) {
      this.address_one = line_one;
      this.area = area;
      this.city = city;
      this.pincode = pincode;
    };
  $scope.address = function(data) {
     var myaddress = new obj_address(data.address_one, data.area, data.city, data.pincode);
     return _.values(myaddress).join(",")
  }
    // Load more function
    $scope.civicissue_load = function() {
      var objs = {
        limit: limit,
        page: civicissue_page_number
      };
      service.post(url + 'get_civicissue', objs).then(function(result) {
        if (result.status == "success") {
          civicissue_page_number = civicissue_page_number + 1;
          if (result.data == 0) {
            $scope.civicissue_loading_icon = false;
            $scope.civicissue_loadmore = false;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
          angular.forEach(result.data, function(item) {
            $scope.civicissue.push(item);
          })
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.civicissue_loading_icon = false;
        } else if (result.status == "error") {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.civicissue_loading_icon = false;
          $cordovaToast.showLongBottom('Network Failed' + result.message);
        }
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        if (error == 500 || error == 404 || error == 0) {
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
    };
    $scope.civicissue_refresh = function() {
      $timeout(function() {
        $scope.civicissue_load_icon = true;
        var civicissue_page_number = 1;
        $scope.civicissue_nodata = false;
        $scope.civicissue_loadmore = false;
        var obj = {
          limit: limit,
          page: civicissue_page_number
        };
        $scope.civicissue_loading_icon = true;
        service.post(url + 'get_civicissue', obj).then(function(result) {
          $scope.civicissue_load_icon = false;
          if (result.status == "success") {
            if (result.data == 0) {
              $scope.civicissue_nodata = true;
              $scope.civicissue_loading_icon = false;
              $scope.civicissue_loadmore = false;
            } else {
              civicissue_page_number = civicissue_page_number + 1;
              $scope.civicissue = result.data;
              $scope.civicissue_loading_icon = true;
              $scope.civicissue_loadmore = true;
            }
          } else if (result.status == "error") {
            $scope.civicissue_loading_icon = false;
            $cordovaToast.showLongBottom('Network Failed' + result.message);
          }
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {
            $scope.civicissue_nodata = true;
            $scope.civicissue_loading_icon = false;
            $scope.civicissue_loadmore = false;
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    }
    $scope.next=function(){
      $state.go('app.directory');
    }
    $scope.prev=function(){
      $state.go('app.event');
    }
    $scope.details=function(x){
        $state.go('details',{'obj':{'page':'notification','category':'Civic Issue','id':x}});
    }
})

app.controller('EventCtrl',function($scope,$state,service,$cordovaToast,$timeout) {
  // Event Issue page
    var event_page_number = 1;
    $scope.event_nodata = false;
    $scope.event_loadmore = false;
    $scope.event_load_icon = true;
    var obj = {
      limit: limit,
      page: event_page_number
    };
    service.post(url + 'get_event', obj).then(function(result) {
      if (result.status == "success") {
        if (result.data == 0) {
          $scope.event_nodata = true;
          $scope.event_loading_icon = false;
          $scope.event_loadmore = false;
          $scope.event_load_icon = false;
        } else {
          $scope.event_load_icon = false;
          event_page_number = event_page_number + 1;
          $scope.events = result.data;
          $scope.event_loading_icon = true;
          $scope.event_loadmore = true;
        }
      } else if (result.status == "error") {
        $scope.event_loading_icon = false;
        $cordovaToast.showLongBottom('Network Failed' + result.message);
      }
    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.event_nodata = true;
        $scope.event_loading_icon = false;
        $scope.event_loadmore = false;
        $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });

    // Load more function
    $scope.event_load = function() {
      $scope.event_loading_icon = true;
      var objs = {
        limit: limit,
        page: event_page_number
      };
      service.post(url + 'get_event', objs).then(function(result) {
        if (result.status == "success") {
          event_page_number = event_page_number + 1;
          if (result.data == 0) {
            $scope.event_loading_icon = false;
            $scope.event_loadmore = false;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
          angular.forEach(result.data, function(item) {
            $scope.events.push(item);
          })
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.event_loading_icon = false;
        } else if (result.status == "error") {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.event_loading_icon = false;
          $cordovaToast.showLongBottom('Network Failed' + result.message);
        }
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        if (error == 500 || error == 404 || error == 0) {
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
    };
    $scope.event_refresh = function() {
      $timeout(function() {
        $scope.event_load_icon = true;
        var event_page_number = 1;
        $scope.event_nodata = false;
        $scope.event_loadmore = false;
        var obj = {
          limit: limit,
          page: event_page_number
        };
        service.post(url + 'get_event', obj).then(function(result) {
          $scope.event_load_icon = false;
          if (result.status == "success") {
            if (result.data == 0) {
              $scope.event_nodata = true;
              $scope.event_loading_icon = false;
              $scope.event_loadmore = false;
            } else {
              event_page_number = event_page_number + 1;
              $scope.events = result.data;
              $scope.$broadcast('scroll.infiniteScrollComplete');
              $scope.event_loading_icon = true;
              $scope.event_loadmore = true;
            }
          } else if (result.status == "error") {
            $scope.event_loading_icon = false;
            $cordovaToast.showLongBottom('Network Failed' + result.message);
          }
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {
            $scope.event_nodata = true;
            $scope.event_loading_icon = false;
            $scope.event_loadmore = false;
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    }
    $scope.next=function(){
      $state.go('app.civic');
    }
    $scope.prev=function(){
      $state.go('app.tourism');
    }
    $scope.details=function(x){
        $state.go('details',{'obj':{'page':'notification','category':'Event','id':x}});
    }
})

app.controller('TourismCtrl', function($scope,$state,service,$cordovaToast,$timeout) {
  // Tourism Issue page
    var tourism_page_number = 1;
    $scope.tourism_nodata = false;
    $scope.tourism_loadmore = false;
    $scope.tourism_load_icon = true;
    var obj = {
      limit: limit,
      page: tourism_page_number
    };
    service.post(url + 'get_tourism', obj).then(function(result) {
      if (result.status == "success") {
        if (result.data == 0) {
          $scope.tourism_nodata = true;
          $scope.tourism_loading_icon = false;
          $scope.tourism_loadmore = false;
          $scope.tourism_load_icon = false;
        } else {
          $scope.tourism_load_icon = false;
          tourism_page_number = tourism_page_number + 1;
          $scope.tourism = result.data;
          $scope.tourism_loading_icon = true;
          $scope.tourism_loadmore = true;
        }
      } else if (result.status == "error") {
        $scope.tourism_loading_icon = false;
        $cordovaToast.showLongBottom('Network Failed' + result.message);
      }
    }, function(error) {
      if (error == 500 || error == 404 || error == 0) {
        $scope.tourism_nodata = true;
        $scope.tourism_loading_icon = false;
        $scope.tourism_loadmore = false;
        $cordovaToast.showLongBottom('Network Failed' + error);
      }
    });

    // Load more function
    $scope.tourism_load = function() {
      $scope.tourism_loading_icon = true;
      var objs = {
        limit: limit,
        page: tourism_page_number
      };
      service.post(url + 'get_tourism', objs).then(function(result) {
        if (result.status == "success") {
          tourism_page_number = tourism_page_number + 1;
          if (result.data == 0) {
            $scope.tourism_loading_icon = false;
            $scope.tourism_loadmore = false;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
          angular.forEach(result.data, function(item) {
            $scope.tourism.push(item);
          })
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.tourism_loading_icon = false;
        } else if (result.status == "error") {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.tourism_loading_icon = false;
          $cordovaToast.showLongBottom('Network Failed' + result.message);
        }
      }, function(error) {
        $scope.$broadcast('scroll.infiniteScrollComplete');
        if (error == 500 || error == 404 || error == 0) {
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
    };

    $scope.tourism_refresh = function() {
      $timeout(function() {
        $scope.tourism_load_icon = true;
        var tourism_page_number = 1;
        $scope.tourism_nodata = false;
        $scope.tourism_loadmore = false;
        var obj = {
          limit: limit,
          page: tourism_page_number
        };
        service.post(url + 'get_tourism', obj).then(function(result) {
          $scope.tourism_load_icon = false;
          if (result.status == "success") {
            if (result.data == 0) {
              $scope.tourism_nodata = true;
              $scope.tourism_loading_icon = false;
              $scope.tourism_loadmore = false;
            } else {
              tourism_page_number = tourism_page_number + 1;
              $scope.tourism = result.data;
              $scope.tourism_loading_icon = true;
              $scope.tourism_loadmore = true;
            }
          } else if (result.status == "error") {
            $scope.tourism_loading_icon = false;
            $cordovaToast.showLongBottom('Network Failed' + result.message);
          }
        }, function(error) {
          if (error == 500 || error == 404 || error == 0) {
            $scope.tourism_nodata = true;
            $scope.tourism_loading_icon = false;
            $scope.tourism_loadmore = false;
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
        //Stop the ion-refresher from spinning
        $scope.$broadcast('scroll.refreshComplete');
      }, 1000);
    }
    $scope.next=function(){
      $state.go('app.event');
    }
    $scope.prev=function(){
      $state.go('app.news');
    }
    $scope.details=function(x){
        $state.go('details',{'obj':{'page':'notification','category':'Tourism','id':x}});
    }
})



app.controller('ContactCtrl', function($scope,$state, $cordovaToast, $ionicLoading, service,$state) {
  // angular.element(document.querySelector("#page1")).removeClass("scroll-content");
  $scope.contact_form_Submit = function(form) {
    if (form) {
      $ionicLoading.show({
        template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
      })
      var obj = {
        'name': form.name,
        'email': form.email,
        'subject': form.subject,
        'message': form.message
      };
      service.post(url + 'get_contact', obj).then(function(result) {
        $ionicLoading.hide();
        if (result.status == "success") {
          this.contact = '';
          $scope.contact = '';
          $cordovaToast.showLongCenter(result.message);
          $state.go('app.news');
        } else if (result.status == "error") {
          $cordovaToast.showLongBottom(result.message);
        }
      }, function(error) {
        $ionicLoading.hide();
        if (error == 500 || error == 404 || error == 0) {
          $scope.news_nodata = true;
          $scope.news_loading_icon = false;
          $scope.news_loadmore = false;
          $cordovaToast.showLongBottom('Network Failed' + error);
        }
      });
    }
  }
})

app.controller('VilambaramCtrl', function($http,$state, $scope, service, $ionicLoading, $cordovaToast, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $ionicActionSheet) {
  // angular.element(document.querySelector("#page2")).removeClass("scroll-content");
  $scope.authorization = {};
  var con = {};
  con.concept = '1';
  this.error_banner_type = false;
  $scope.authorization = con;
  $scope.delete_image = function() {
    $scope.model_image = '';
  }
  $scope.request_form_Submit = function(form) {
    if (form.banner_type.static == false && form.banner_type.animation == false) {
      this.error_banner_type = true;
    }
    if (form) {
      form.model_image=$scope.model_image
      $ionicLoading.show({
        template: '<ion-spinner icon="ios" class="spinner-assertive"></ion-spinner>'
      })
      // Destination URL
      var urls = url + 'upload_ad_request_image';
      // File for Upload
      var targetPath = $scope.pathForImage($scope.model_image);
      // File name only
      var options = {
        fileKey: "file",
        fileName: $scope.model_image,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params: {
          "name": form.name,
           "company": form.company,
           "mobile": form.mobile,
           "email": form.email,
           "website": form.website,
           "banner_type": form.banner_type,
           "bannersize": form.bannersize,
           "content": form.content,
           "description": form.description,
           "comments": form.comments,
           "concept": form.concept,
           "model_image": $scope.model_image
        }
      };
      if($scope.model_image){
        $cordovaFileTransfer.upload(url+'request_advertise', targetPath, options).then(function(result) {
          $ionicLoading.hide();
          $cordovaToast.showLongCenter("hi");
          alert(JSON.stringify(result.response));
          $state.go('app.news');
          $cordovaToast.showLongCenter("hi");
          }, function(error) {
            $cordovaToast.showLongBottom('Error : ' + error.exception);
              $ionicLoading.hide();
          });
      }else{
        service.post(url + 'request_advertise', form).then(function(result) {
          $ionicLoading.hide();
          if (result.status == "success") {
            $cordovaToast.showLongCenter(result.message);
            this.authorization = '';
            $scope.authorization='';
            $scope.model_image='';
            this.model_image = '';
            $state.go('app.news');
          } else if (result.status == "error") {
            $cordovaToast.showLongBottom(result.message);
          }
        }, function(error) {
          $ionicLoading.hide();
          if (error == 500 || error == 404 || error == 0) {
            $scope.news_nodata = true;
            $scope.news_loading_icon = false;
            $scope.news_loadmore = false;
            $cordovaToast.showLongBottom('Network Failed' + error);
          }
        });
      }


    }
  }
  $scope.loadImage = function() {
    var showActionSheet = $ionicActionSheet.show({
      buttons: [{
          text: 'Load from Library'
        },
        {
          text: 'Use Camera'
        }
      ],
      // destructiveText: 'Delete',
      titleText: 'Select Image Source',
      cancelText: 'Cancel',
      cancel: function() {

      },

      buttonClicked: function(index) {
        var type = null;
        if (index === 0) {
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        }
        if (index === 1) {
          type = Camera.PictureSourceType.CAMERA;
        }
        if (type !== null) {
          $scope.selectPicture(type);
        }
        return true;
      },

      destructiveButtonClicked: function() {
        // add delete code..
      }
    });
  };
  // Returns the local path inside the app for an image
  $scope.pathForImage = function(image) {
    if (image === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + image;
    }
  };
  var temp = {};
  $scope.selectPicture = function(sourceType) {
    var options = {
      quality: 100,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: sourceType,
      saveToPhotoAlbum: false
    };

    $cordovaCamera.getPicture(options).then(function(imagePath) {
        // Grab the file name of the photo in the temporary directory
        var currentName = imagePath.replace(/^.*[\\\/]/, '');

        //Create a new name for the photo
        var d = new Date(),
          n = d.getTime(),
          newFileName = n + ".jpg";

        // If you are trying to load image from the gallery on Android we need special treatment!
        if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
          window.FilePath.resolveNativePath(imagePath, function(entry) {
            window.resolveLocalFileSystemURL(entry, success, fail);

            function fail(e) {
              console.error('Error: ', e);
            }

            function success(fileEntry) {
              var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
              // Only copy because of access rights
              $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success) {
                $scope.model_image = newFileName;
              }, function(error) {
                $cordovaToast.showLongBottom('Error : ' + error.exception);
              });
            };
          });
        } else {
          var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
          // Move the file to permanent storage
          $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success) {
            $scope.model_image = newFileName;
          }, function(error) {
            $cordovaToast.showLongBottom('Error : ' + error.exception);
          });
        }
      },
      function(err) {
        // Not always an error, maybe cancel was pressed...
      })
  };
});

app.controller('SettingsCtrl', function($scope,$ionicHistory,acepush,browser) {

  var get_push = '';
  var get_size = '';
  if (localStorage.getItem('setting')) {
    if (JSON.parse(localStorage.getItem('setting')).alert == true) {
      $scope.push = true;
    } else {
      $scope.push = false;
    }
    $scope.font = JSON.parse(localStorage.getItem('setting')).font_size;
  }
  //cache view
  $scope.clear = function() {
    var message = "Are you sure to clear memory ?";
    var title = "MylaporeToday";
    var buttonLabels = ["YES", "NO"];
    navigator.notification.confirm(message, confirmCallback, title, buttonLabels);
    function confirmCallback(buttonIndex) {
      if (buttonIndex == 1) {
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        localStorage.clear();
      }
    }
  }
  $scope.openBrowser = function(link) {
    browser.open(link);
  }
  $scope.font_size = function(size) {
    $scope.font = size;
    get_push = JSON.parse(localStorage.getItem('setting'));
    set = {
      'init': true,
      'alert': get_push.alert,
      'font_size': size
    };
    localStorage.setItem('setting', JSON.stringify(set));
  }

  $scope.setting = function() {
    $scope.push = !$scope.push;
    get_size = JSON.parse(localStorage.getItem('setting'));
    set = {
      'init': true,
      'alert': $scope.push,
      'font_size': get_size.font_size
    };
    localStorage.setItem('setting', JSON.stringify(set));
    var obj = {
      'app_id': app_id,
      'device_token': device_token,
      'model': model.model,
      'platform': model.platform,
      'status': $scope.push
    }
    acepush.notification(obj);
  }
});


app.controller('DetailsCtrl', function($ionicModal,service,$ionicSlideBoxDelegate,$cordovaSocialSharing,$ionicLoading, $compile, $rootScope, $scope, scroll, $stateParams, $ionicScrollDelegate, $ionicSlideBoxDelegate, $window) {


  $scope.urls = url;
  $scope.iurls = image_url;
  var datas=$stateParams.obj;
    $scope.otherimage=[];
  $scope.mytitle=datas.category;
  var obj = {
    'category': datas.category,
    'id':datas.id
  }
  $scope.page=datas.category;
  $scope.notificationdetail_load_icon=true;
  service.post(url + 'get_notification_detail', obj).then(function(result) {
    if (result.status == "success") {
      $scope.detail = result.data;
      $ionicSlideBoxDelegate.update();
      if(datas.category !='Tourism'){
        $scope.otherimage.push($scope.iurls + $scope.detail.image_path + '/' + $scope.detail.image);
      }else{
        angular.forEach($scope.detail.image, function(item) {
          $scope.otherimage.push($scope.iurls + item);
        })
      }
      $scope.notificationdetail_load_icon=false;
    } else if (result.status == "error") {
      $scope.notification_load_icon=false;
    }
  }, function(error) {
    if (error == 500 || error == 404 || error == 0) {
    }
  });
  // $scope.otherimage.push(image_url + $scope.detail.image_path + '/' + $scope.detail.image);
  $scope.zoomMin = 1;
  $scope.showImages = function() {
    $scope.showModal('templates/gallery-zoomview.html');
  };
  $scope.multishowImages = function(index) {
    $scope.activeSlide = index;
    $scope.showModal('templates/multigallery-zoomview.html');
  };
  function getPosition(element) {
    var xPosition = 0;
    var yPosition = 0;

    while (element) {
      xPosition += (element.offsetLeft - element.scrollLeft + element.clientLeft);
      yPosition += (element.offsetTop - element.scrollTop + element.clientTop);
      element = element.offsetParent;
    }
    return {
      x: xPosition,
      y: yPosition
    };
  }
  $scope.getTouchposition = function(event) {
    var canvasPosition = getPosition(event.gesture.touches[0].target);
    var tap = {
      x: 0,
      y: 0
    };
    if (event.gesture.touches.length > 0) {
      tt = event.gesture.touches[0];
      tap.x = tt.clientX || tt.pageX || tt.screenX || 0;
      tap.y = tt.clientY || tt.pageY || tt.screenY || 0;
    }
    tap.x = tap.x - canvasPosition.x;
    tap.y = tap.y - canvasPosition.y;

    return {
      x: tap.x,
      y: tap.y
    };
  }
  var zoomed = true;
  $scope.onDoubleTap = function(a) {
    if (zoomed) { // toggle zoom in
      var tap = {
        x: 0,
        y: 0
      };
      var position = $scope.getTouchposition(event);
      $ionicScrollDelegate.$getByHandle('scrollHandle' + a).zoomBy(2.0, true, position.x, position.y);
      zoomed = !zoomed;
    } else { // toggle zoom out
      $ionicScrollDelegate.$getByHandle('scrollHandle' + a).zoomTo(1, true);
      zoomed = !zoomed;
    }
  }

  $scope.showModal = function(templateUrl) {
    $ionicModal.fromTemplateUrl(templateUrl, {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });
  }

  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };

  $scope.updateSlideStatus = function(slide) {
    var zoomFactor = $ionicScrollDelegate.$getByHandle('scrollHandle' + slide).getScrollPosition().zoom;
    if (zoomFactor == $scope.zoomMin) {
      $ionicSlideBoxDelegate.enableSlide(true);
    } else {
      $ionicSlideBoxDelegate.enableSlide(false);
    }
  };

  $scope.urls = url;
    $scope.iurls = image_url;
    /*$scope.surl = share_url;*/
    var share_url;
var cat=datas.category;
if( cat == 'News')
{
share_url = 'https://www.mylaporetoday.com/local-area-news-details/';
}
else if( cat == 'Event')
{
share_url = 'https://www.mylaporetoday.com/event_sub/'
}
else if( cat == 'Civic Issue')
{
share_url = 'https://www.mylaporetoday.com/civic-issues-details/'
}
else if( cat == 'Tourism')
{
share_url = 'https://www.mylaporetoday.com/tourism_sub/'
}
String.prototype.trunc = String.prototype.trunc ||
function(n){

    // this will return a substring and 
    // if its larger than 'n' then truncate and append '...' to the string and return it.
    // if its less than 'n' then return the 'string'
    return this.length>n ? this.substr(0,n-1)+'...' : this.toString();
};
    $scope.share = function(data) {


      if( cat == 'Tourism')
      {
      $cordovaSocialSharing
        .share(data.description.trunc(100) , data.title , $scope.iurls + data.image , share_url + data.slug) // Share via native share sheet
        .then(function(result) {
          // $cordovaToast.showLongBottom('Status : ' + result);
        }, function(err) {
          // $cordovaToast.showLongBottom('Status : ' + err);
        });
    }
    else{
      $cordovaSocialSharing
        .share(data.description.trunc(100) , data.title , $scope.iurls + data.image_path + '/' + data.image , share_url + data.slug) // Share via native share sheet
        .then(function(result) {
          // $cordovaToast.showLongBottom('Status : ' + result);
        }, function(err) {
          // $cordovaToast.showLongBottom('Status : ' + err);
        });
    }
  }
    $scope.sharewhatsapp = function(data)
    {
      if( cat == 'Tourism')
      {
      $cordovaSocialSharing
    .shareViaWhatsApp(data.description.trunc(100) , $scope.iurls + data.image , share_url + data.slug)
    .then(function(result) {
      // Success!
    }, function(err) {
      // An error occurred. Show a message to the user
    });
}
else
{
  $cordovaSocialSharing
    .shareViaWhatsApp(data.description.trunc(100) , $scope.iurls + data.image , share_url + data.slug)
    .then(function(result) {
      // Success!
    }, function(err) {
      // An error occurred. Show a message to the user
    });
}
    }
});