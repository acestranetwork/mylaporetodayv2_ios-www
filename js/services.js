app.factory('acepush', function(service) {
  return {
    notification: function(obj) {
      service.post('http://push.mylaporetoday.in/device_register', obj).then(function(result) {
        if (result.status == "success") {
        } else if (result.status == "error") {
          console.log(result.message);
        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
    },
  };
});

app.factory('mylaipush', function(service) {
  return {
    notification: function(obj) {
      service.post(url+'device_register',obj).then(function(result) {
        if (result.status == "success") {
        }
      },function(error){
        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
    }
  }
});

app.directive('file', function() {
  return {
    scope: {
      file: '='
    },
    link: function(scope, el, attrs) {
      el.bind('change', function(event) {
        var file = event.target.files[0];
        scope.file = file ? file : undefined;
        scope.$apply();
      });
    }
  };
});

app.service('$storage', function() {
// For the purpose of this exampe I will store user data on ionic local storage but you should save it on a database
var set = function(value) {
  window.localStorage.storage = JSON.stringify(value);
};
var get = function(){
  return JSON.parse(window.localStorage.storage || '{}');
};
return {
  get: get,
  set: set
};
})

app.factory('data', function($storage) {
  var temp = [];
  var obtained_array = [];
  return {
    add: function (data) {
    var getdata=$storage.get();
    if(JSON.stringify(getdata) == "{}"){
      obtained_array.push(data)
      $storage.set(obtained_array);
    }else{
      var j=0;
      for (var i = 0; i < getdata.length; i++) {
        if(getdata[i].id== data.id){
          // getdata[i].val++;
          $storage.set(getdata);
          j++;
        }
      }
      if(j==0){
        getdata.push(data);
        $storage.set(getdata);
      }
    }
  },
  get:function(data){
    var k=0;
    var result='';
    var somedata=$storage.get();
    for (var i = 0; i < somedata.length; i++) {
      if(somedata[i].id== data){
         result=data;
         k++;
      }
    }
    if(k==0){
       result=false;
    }
    return result;
  },
  delete:function(){
    var removedata=$storage.get();
    for (var i = 0; i < removedata.length; i++) {
        var now = moment(new Date()); //todays date
        var end = moment(removedata[i].date); // another date
        var duration = moment.duration(now.diff(end));
        var days = duration.asDays();
        if(Math.round(days) > 5)
        {
          removedata.splice(removedata[i],1);
        }
    }
    $storage.set(removedata);
  }
  };
});
// app.factory('data', function() {
//   var temp = [];
//   var set = function(x, y) {
//     window.localStorage[x] = JSON.stringify(y);
//   };
//   var get = function(x) {
//     return JSON.parse(window.localStorage[x] || '{}');
//   };
//   return {
//     init: function(obj, value) {
//       return set(obj, value);
//     },
//     all: function(obj) {
//       return get(obj) || 0;
//     },
//     push: function(obj, value) {
//       temp = get(obj);
//       temp.push(value);
//       return set(obj, temp);
//       // return set(obj,get(obj).push(value));
//     },
//     get: function(obj, id) {
//       for (var i = 0; i < get(obj).length; i++) {
//         if (get(obj)[i].id === parseInt(id)) {
//           return get(obj)[i];
//         }
//       }
//     },
//   };
// });

app.factory('object', function() {
  var a=[],b=[],c=[],d=[],e=[],f=[];
  return {
    clear: function() {
       a=[],b=[],c=[],d=[],e=[],f=[];
    },
    set: function(category,value) {
      if(category == 'News'){
        a=[];
        a=value;
      }else if(category == 'Classified'){
        b=[];
        b=value;
      }else if(category == 'Directory'){
        c=[];
        c=value;
      }else if(category == 'Civic Issue'){
        d=[];
        d=value;
      }else if(category == 'Event'){
        e=[];
        e=value;
      }else if(category == 'Tourism'){
        f=[];
        f=value;
      }
    },
    add: function(category,value) {
      if(category == 'News'){
        a.push(value);
      }else if(category == 'Classified'){
        b.push(value);
      }else if(category == 'Directory'){
        c.push(value);
      }else if(category == 'Civic Issue'){
        d.push(value);
      }else if(category == 'Event'){
        e.push(value);
      }else if(category == 'Tourism'){
        f.push(value);
      }
    },
    get: function(category,id) {
      if(category == 'News'){
        for (var i = 0; i < a.length; i++) {
          if (a[i].id === parseInt(id)) {
            return a[i];
          }
        }
        }else if(category == 'Classified'){
        for (var j = 0; j < b.length; j++) {
          if (b[j].id === parseInt(id)) {
            return b[j];
          }
        }
      }else if(category == 'Directory'){
        for (var k = 0; k < c.length; k++) {
          if (c[k].id === parseInt(id)) {
            return c[k];
          }
        }
      }else if(category == 'Civic Issue'){
        for (var l = 0; l < d.length; l++) {
          if (d[l].id === parseInt(id)) {
            return d[l];
          }
        }
      }else if(category == 'Event'){
        for (var m = 0; m < e.length; m++) {
          if (e[m].id === parseInt(id)) {
            return e[m];
          }
        }
      }else if(category == 'Tourism'){
        for (var n = 0; n < f.length; n++) {
          if (f[n].id === parseInt(id)) {
            return f[n];
          }
        }
      }
    },
  };
});

app.service('SchoolInfo', function() {
  // For the purpose of this example I will store user data on ionic local storage but you should save it on a database
  var set = function(data) {
    window.localStorage.UserInfo = JSON.stringify(data);
  };
  var get = function() {
    return JSON.parse(window.localStorage.UserInfo || '{}');
  };
  return {
    get: get,
    set: set
  };
});

app.factory('scroll', function() {
  var temps = 0;
  return {
    put: function(value) {
      return temps = value;
    },
    get: function() {
      return temps || 0;
    }
  };
});

app.factory('service', function($http, $q, $window) {
  return {
    get: function(url) {
      var deferred = $q.defer();
      $http.get(url)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(msg, code) {
          deferred.reject(code);
        });
      return deferred.promise;
    },
    post: function(url, obj) {
      var deferred = $q.defer();
      $http.post(url, obj)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(msg, code) {
          deferred.reject(code);
        });
      return deferred.promise;
    }
  };
});

app.factory('browser', function($cordovaInAppBrowser, $rootScope) {
  return {
    open: function(obj) {
      var options = {
        location: 'yes',
        clearcache: 'yes',
        toolbar: 'no',
        closebuttoncaption: 'Back',
        transitionstyle: 'crossdissolve'
      };
      $cordovaInAppBrowser.open(obj, '_blank', options)
        .then(function(event) {
          console.log("success");
        })
        .catch(function(event) {
          console.log("failure");
        });
    }
  }
});

app.filter('unique', function() {
  // we will return a function which will take in a collection
  // and a keyname
  return function(collection, keyname) {
    // we define our output and keys array;
    var output = [],
      keys = [];

    // we utilize angular's foreach function
    // this takes in our original collection and an iterator function
    angular.forEach(collection, function(item) {
      // we check to see whether our object exists
      var key = item[keyname];
      // if it's not already part of our keys array
      if (keys.indexOf(key) === -1) {
        // add it to our keys array
        keys.push(key);
        // push this item to our final output array
        output.push(item);
      }
    });
    // return our array which should be devoid of
    // any duplicates
    return output;
  };
});

// app.filter('timeago', function() {
//         return function(input, p_allowFuture) {
//
//             var substitute = function (stringOrFunction, number, strings) {
//                     var string = angular.isFunction(stringOrFunction) ? stringOrFunction(number, dateDifference) : stringOrFunction;
//                     var value = (strings.numbers && strings.numbers[number]) || number;
//                     return string.replace(/%d/i, value);
//                 },
//                 nowTime = (new Date()).getTime(),
//                 date = (new Date(input)).getTime(),
//                 //refreshMillis= 6e4, //A minute
//                 allowFuture = p_allowFuture || false,
//                 strings= {
//                     prefixAgo: '',
//                     prefixFromNow: '',
//                     suffixAgo: "ago",
//                     suffixFromNow: "from now",
//                     seconds: "less than a minute",
//                     minute: "about a minute",
//                     minutes: "%d minutes",
//                     hour: "about an hour",
//                     hours: "about %d hours",
//                     day: "a day",
//                     days: "%d days",
//                     month: "about a month",
//                     months: "%d months",
//                     year: "about a year",
//                     years: "%d years"
//                 },
//                 dateDifference = nowTime - date,
//                 words,
//                 seconds = Math.abs(dateDifference) / 1000,
//                 minutes = seconds / 60,
//                 hours = minutes / 60,
//                 days = hours / 24,
//                 years = days / 365,
//                 separator = strings.wordSeparator === undefined ?  " " : strings.wordSeparator,
//
//
//                 prefix = strings.prefixAgo,
//                 suffix = strings.suffixAgo;
//
//             if (allowFuture) {
//                 if (dateDifference < 0) {
//                     prefix = strings.prefixFromNow;
//                     suffix = strings.suffixFromNow;
//                 }
//             }
//
//             words = seconds < 45 && substitute(strings.seconds, Math.round(seconds), strings) ||
//             seconds < 90 && substitute(strings.minute, 1, strings) ||
//             minutes < 45 && substitute(strings.minutes, Math.round(minutes), strings) ||
//             minutes < 90 && substitute(strings.hour, 1, strings) ||
//             hours < 24 && substitute(strings.hours, Math.round(hours), strings) ||
//             hours < 42 && substitute(strings.day, 1, strings) ||
//             days < 30 && substitute(strings.days, Math.round(days), strings) ||
//             days < 45 && substitute(strings.month, 1, strings) ||
//             days < 365 && substitute(strings.months, Math.round(days / 30), strings) ||
//             years < 1.5 && substitute(strings.year, 1, strings) ||
//             substitute(strings.years, Math.round(years), strings);
//      console.log(prefix+words+suffix+separator);
//      prefix.replace(/ /g, '')
//      words.replace(/ /g, '')
//      suffix.replace(/ /g, '')
//      return (prefix+' '+words+' '+suffix+' '+separator);
//
//         };
//     });
app.directive('lazyScroll', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      restrict: 'A',
      link: function($scope, $element) {

        var scrollTimeoutId = 0;

        $scope.invoke = function() {
          $rootScope.$broadcast('lazyScrollEvent');
        };
        $element.bind('scroll', function() {
          $timeout.cancel(scrollTimeoutId);
          // wait and then invoke listeners (simulates stop event)
          scrollTimeoutId = $timeout($scope.invoke, 0);
        });
      }
    };
  }
])

app.directive('imageLazySrc', ['$document', '$timeout', '$ionicScrollDelegate', '$compile',
  function($document, $timeout, $ionicScrollDelegate, $compile) {
    return {
      restrict: 'A',
      scope: {
        lazyScrollResize: "@lazyScrollResize",
        imageLazyBackgroundImage: "@imageLazyBackgroundImage"
      },
      link: function($scope, $element, $attributes) {
        if (!$attributes.imageLazyDistanceFromBottomToLoad) {
          $attributes.imageLazyDistanceFromBottomToLoad = 0;
        }
        if (!$attributes.imageLazyDistanceFromRightToLoad) {
          $attributes.imageLazyDistanceFromRightToLoad = 0;
        }

        if ($attributes.imageLazyLoader) {
          var loader = $compile('<div class="image-loader-container"><ion-spinner class="image-loader" icon="' + $attributes.imageLazyLoader + '"></ion-spinner></div>')($scope);
          $element.after(loader);
        }

        var deregistration = $scope.$on('lazyScrollEvent', function() {
          //console.log('scroll');
          if (isInView()) {
            loadImage();
            deregistration();
          }
        });

        function loadImage() {
          //Bind "load" event
          $element.bind("load", function(e) {
            if ($attributes.imageLazyLoader) {
              loader.remove();
            }
            if ($scope.lazyScrollResize == "true") {
              //Call the resize to recalculate the size of the screen
              $ionicScrollDelegate.resize();
            }
          });

          if ($scope.imageLazyBackgroundImage == "true") {
            var bgImg = new Image();
            bgImg.onload = function() {
              if ($attributes.imageLazyLoader) {
                loader.remove();
              }
              $element[0].style.backgroundImage = 'url(' + $attributes.imageLazySrc + ')'; // set style attribute on element (it will load image)
              if ($scope.lazyScrollResize == "true") {
                //Call the resize to recalculate the size of the screen
                $ionicScrollDelegate.resize();
              }
            };
            bgImg.src = $attributes.imageLazySrc;
          } else {
            $element[0].src = $attributes.imageLazySrc; // set src attribute on element (it will load image)
          }
        }

        function isInView() {
          var clientHeight = $document[0].documentElement.clientHeight;
          var clientWidth = $document[0].documentElement.clientWidth;
          var imageRect = $element[0].getBoundingClientRect();
          return (imageRect.top >= 0 && imageRect.top <= clientHeight + parseInt($attributes.imageLazyDistanceFromBottomToLoad)) &&
            (imageRect.left >= 0 && imageRect.left <= clientWidth + parseInt($attributes.imageLazyDistanceFromRightToLoad));
        }

        // bind listener
        // listenerRemover = scrollAndResizeListener.bindListener(isInView);

        // unbind event listeners if element was destroyed
        // it happens when you change view, etc
        $element.on('$destroy', function() {
          deregistration();
        });

        // explicitly call scroll listener (because, some images are in viewport already and we haven't scrolled yet)
        $timeout(function() {
          if (isInView()) {
            loadImage();
            deregistration();
          }
        }, 500);
      }
    };
  }
]);

app.value('OverflowMarqueeValues', {
  overflowMarqueeClass: "overflow-marquee",
  parentMarqueeClass: "overflow-marquee-container",
  intervalDuration: 25,
  defaultMarqueeSpeed: 1,
  statusCodes: {
    STARTING: 1,
    STARTED: 100,
    PAUSING: 0,
    PAUSED: -1
  },
  opacitySpeed: 500,
  resetSpeed: 400
})

app.directive('overflowMarquee', ['$timeout', '$interval', '$q', 'OverflowMarqueeValues', function($timeout, $interval, $q, OverflowMarqueeValues) {
  return {
    restrict: 'A',
    scope: {
      overflowMarqueePause: '=?', // Two-way bind, to allow pause and unpause scroll
      overflowMarqueeSpeed: '=?', // Optional: Pixels per iteration. Default 1.
      overflowMarqueeRepeat: '=?' // Optional: Number of times to repeat. Set -1 for infinite. Default infinite.
    },
    link: function($scope, $element, $attrs) {
      // Add marquee classes to simulate overflow (if any)
      $element.addClass(OverflowMarqueeValues.overflowMarqueeClass);
      $element.parent().addClass(OverflowMarqueeValues.parentMarqueeClass);

      var loop;
      var resetTimeout1, resetTimeout2, resetTimeout3;
      var pauseWatcher;
      var stopping = $q.when();

      // Allow one digest cycle before start
      var start = $timeout(function() {
        if ($element.width() > $element.parent().width()) {
          $scope.overflowMarqueeSpeed = parseInt($scope.overflowMarqueeSpeed) || OverflowMarqueeValues.defaultMarqueeSpeed;
          $scope.overflowMarqueePause = $scope.overflowMarqueePause || false;
          $scope.status = OverflowMarqueeValues.statusCodes.PAUSED;
          $scope.moved = 0;
          $scope.overflowLength = $element.width() - $element.parent().width();

          pauseWatcher = $scope.$watch('overflowMarqueePause', function(pause) {
            if (pause) {
              reset();
            } else {
              // Wait for reset to finish
              if (stopping.$$state.status === 0) {
                stopping.then(displayFullTitle);
              } else {
                displayFullTitle();
              }
            }
          });
        } else {
          $element.removeClass(OverflowMarqueeValues.titleMarqueeClass);
          $element.parent().removeClass(OverflowMarqueeValues.parentMarqueeClass);
        }
      });

      function reset() {
        $scope.status = OverflowMarqueeValues.statusCodes.PAUSING;
        $scope.moved = 0;
        $interval.cancel(loop);

        if (stopping.$$state.status != 0) {
          var deferred = $q.defer();

          $element.css('opacity', 0);
          var resetTimeout1 = $timeout(function() {
            $element.css('left', 0);
            resetTimeout2 = $timeout(function() {
              $element.css('opacity', 1);
              resetTimeout3 = $timeout(function() {
                $scope.status = OverflowMarqueeValues.statusCodes.PAUSED;
                $timeout.cancel(resetTimeout1);
                $timeout.cancel(resetTimeout2);
                $timeout.cancel(resetTimeout3);

                deferred.resolve();
                // stopping = null;
              }, OverflowMarqueeValues.resetSpeed);
            }, OverflowMarqueeValues.opacitySpeed);
          }, OverflowMarqueeValues.opacitySpeed);

          stopping = deferred.promise;
        }
        return stopping;
      }

      function displayFullTitle() {
        if (loop && loop.$$state.status === 0) {
          return;
        }
        $scope.status = OverflowMarqueeValues.statusCodes.STARTING;
        loop = $interval(updateFunction, OverflowMarqueeValues.intervalDuration);
      }

      function updateFunction() {
        $scope.status = OverflowMarqueeValues.statusCodes.STARTED;
        if ($scope.moved < $scope.overflowLength) {
          // Continue shifting element
          $scope.moved += $scope.overflowMarqueeSpeed;
          $element.css('left', parseInt($element.css('left')) - $scope.overflowMarqueeSpeed);
        } else {
          // Stop loop. Hide and reset before restarting.
          reset().then(function() {
            if (!$scope.overflowMarqueePause) {
              displayFullTitle();
            }
          });
        }
      }

      $element.on('destroy', function() {
        pauseWatcher();
        $timeout.cancel(start);
        $timeout.cancel(resetTimeout1);
        $timeout.cancel(resetTimeout2);
        $timeout.cancel(resetTimeout3);
        $interval.cancel(loop);
        $scope.$destroy();
      });
    }
  }
}]);
var isArray = angular.isArray;
var forEach = angular.forEach;
var isString = angular.isString;
var jqLite = angular.element;
app.directive('ngMessages', ['$animate', function($animate) {
  var ACTIVE_CLASS = 'ng-active';
  var INACTIVE_CLASS = 'ng-inactive';

  return {
    require: 'ngMessages',
    restrict: 'AE',
    controller: ['$element', '$scope', '$attrs', function($element, $scope, $attrs) {
      var ctrl = this;
      var latestKey = 0;

      var messages = this.messages = {};
      var renderLater, cachedCollection;

      this.render = function(collection) {
        collection = collection || {};

        renderLater = false;
        cachedCollection = collection;

        // this is true if the attribute is empty or if the attribute value is truthy
        var multiple = isAttrTruthy($scope, $attrs.ngMessagesMultiple) ||
          isAttrTruthy($scope, $attrs.multiple);

        var unmatchedMessages = [];
        var matchedKeys = {};
        var messageItem = ctrl.head;
        var messageFound = false;
        var totalMessages = 0;

        // we use != instead of !== to allow for both undefined and null values
        while (messageItem != null) {
          totalMessages++;
          var messageCtrl = messageItem.message;

          var messageUsed = false;
          if (!messageFound) {
            forEach(collection, function(value, key) {
              if (!messageUsed && truthy(value) && messageCtrl.test(key)) {
                // this is to prevent the same error name from showing up twice
                if (matchedKeys[key]) return;
                matchedKeys[key] = true;

                messageUsed = true;
                messageCtrl.attach();
              }
            });
          }

          if (messageUsed) {
            // unless we want to display multiple messages then we should
            // set a flag here to avoid displaying the next message in the list
            messageFound = !multiple;
          } else {
            unmatchedMessages.push(messageCtrl);
          }

          messageItem = messageItem.next;
        }

        forEach(unmatchedMessages, function(messageCtrl) {
          messageCtrl.detach();
        });

        unmatchedMessages.length !== totalMessages ?
          $animate.setClass($element, ACTIVE_CLASS, INACTIVE_CLASS) :
          $animate.setClass($element, INACTIVE_CLASS, ACTIVE_CLASS);
      };

      $scope.$watchCollection($attrs.ngMessages || $attrs['for'], ctrl.render);

      this.reRender = function() {
        if (!renderLater) {
          renderLater = true;
          $scope.$evalAsync(function() {
            if (renderLater) {
              cachedCollection && ctrl.render(cachedCollection);
            }
          });
        }
      };

      this.register = function(comment, messageCtrl) {
        var nextKey = latestKey.toString();
        messages[nextKey] = {
          message: messageCtrl
        };
        insertMessageNode($element[0], comment, nextKey);
        comment.$$ngMessageNode = nextKey;
        latestKey++;

        ctrl.reRender();
      };

      this.deregister = function(comment) {
        var key = comment.$$ngMessageNode;
        delete comment.$$ngMessageNode;
        removeMessageNode($element[0], comment, key);
        delete messages[key];
        ctrl.reRender();
      };

      function findPreviousMessage(parent, comment) {
        var prevNode = comment;
        var parentLookup = [];
        while (prevNode && prevNode !== parent) {
          var prevKey = prevNode.$$ngMessageNode;
          if (prevKey && prevKey.length) {
            return messages[prevKey];
          }

          // dive deeper into the DOM and examine its children for any ngMessage
          // comments that may be in an element that appears deeper in the list
          if (prevNode.childNodes.length && parentLookup.indexOf(prevNode) == -1) {
            parentLookup.push(prevNode);
            prevNode = prevNode.childNodes[prevNode.childNodes.length - 1];
          } else {
            prevNode = prevNode.previousSibling || prevNode.parentNode;
          }
        }
      }

      function insertMessageNode(parent, comment, key) {
        var messageNode = messages[key];
        if (!ctrl.head) {
          ctrl.head = messageNode;
        } else {
          var match = findPreviousMessage(parent, comment);
          if (match) {
            messageNode.next = match.next;
            match.next = messageNode;
          } else {
            messageNode.next = ctrl.head;
            ctrl.head = messageNode;
          }
        }
      }

      function removeMessageNode(parent, comment, key) {
        var messageNode = messages[key];

        var match = findPreviousMessage(parent, comment);
        if (match) {
          match.next = messageNode.next;
        } else {
          ctrl.head = messageNode.next;
        }
      }
    }]
  };

  function isAttrTruthy(scope, attr) {
    return (isString(attr) && attr.length === 0) || //empty attribute
      truthy(scope.$eval(attr));
  }

  function truthy(val) {
    return isString(val) ? val.length : !!val;
  }
}])


app.directive('ngMessagesInclude', ['$templateRequest', '$document', '$compile', function($templateRequest, $document, $compile) {

  return {
    restrict: 'AE',
    require: '^^ngMessages', // we only require this for validation sake
    link: function($scope, element, attrs) {
      var src = attrs.ngMessagesInclude || attrs.src;
      $templateRequest(src).then(function(html) {
        $compile(html)($scope, function(contents) {
          element.after(contents);

          // the anchor is placed for debugging purposes
          var anchor = jqLite($document[0].createComment(' ngMessagesInclude: ' + src + ' '));
          element.after(anchor);

          // we don't want to pollute the DOM anymore by keeping an empty directive element
          element.remove();
        });
      });
    }
  };
}])


app.directive('ngMessage', ngMessageDirectiveFactory('AE'))

app.directive('ngMessageExp', ngMessageDirectiveFactory('A'));

function ngMessageDirectiveFactory(restrict) {
  return ['$animate', function($animate) {
    return {
      restrict: 'AE',
      transclude: 'element',
      terminal: true,
      require: '^^ngMessages',
      link: function(scope, element, attrs, ngMessagesCtrl, $transclude) {
        var commentNode = element[0];

        var records;
        var staticExp = attrs.ngMessage || attrs.when;
        var dynamicExp = attrs.ngMessageExp || attrs.whenExp;
        var assignRecords = function(items) {
          records = items ?
            (isArray(items) ?
              items :
              items.split(/[\s,]+/)) :
            null;
          ngMessagesCtrl.reRender();
        };

        if (dynamicExp) {
          assignRecords(scope.$eval(dynamicExp));
          scope.$watchCollection(dynamicExp, assignRecords);
        } else {
          assignRecords(staticExp);
        }

        var currentElement, messageCtrl;
        ngMessagesCtrl.register(commentNode, messageCtrl = {
          test: function(name) {
            return contains(records, name);
          },
          attach: function() {
            if (!currentElement) {
              $transclude(scope, function(elm) {
                $animate.enter(elm, null, element);
                currentElement = elm;

                // in the event that the parent element is destroyed
                // by any other structural directive then it's time
                // to deregister the message from the controller
                currentElement.on('$destroy', function() {
                  if (currentElement) {
                    ngMessagesCtrl.deregister(commentNode);
                    messageCtrl.detach();
                  }
                });
              });
            }
          },
          detach: function() {
            if (currentElement) {
              var elm = currentElement;
              currentElement = null;
              $animate.leave(elm);
            }
          }
        });
      }
    };
  }];

  function contains(collection, key) {
    if (collection) {
      return isArray(collection) ?
        collection.indexOf(key) >= 0 :
        collection.hasOwnProperty(key);
    }
  }
}

app.factory('Price', function($http, $log, $q) {
  return {
   get: function(url) {
     var deferred = $q.defer();
     $http.get(url)
     .success(function(data) {
      deferred.resolve(data);
    }).error(function(msg, code) {
      deferred.reject(msg);
      $log.error(msg, code);
    });
    return deferred.promise;
  },
  post: function(url,obj) {
   var deferred = $q.defer();
   $http.post(url,obj)
   .success(function(data) {
    deferred.resolve(data);
  }).error(function(msg, code) {
    deferred.reject(msg);
    $log.error(msg, code);
  });
  return deferred.promise;
}
};
});
